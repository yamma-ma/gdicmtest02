CXX=gcc
TARGET=GDICMTest02.exe
OBJ=$(TARGET:%.exe=%.o)
RESOURCE=$(TARGET:%.exe=%.res.o)
CXXFLAGS=-Wno-multichar -Wno-write-strings -DUNICODE
LIBS=-lstdc++ -lmscms -lgdi32

all: $(TARGET)

.PHONY: clean

$(TARGET): $(RESOURCE) $(OBJ)
	$(CXX) -o $@ $(RESOURCE) $(OBJ) $(LIBS) -mwindows

$(RESOURCE): $(TARGET:%.exe=%.rc)
	windres -o $@ $<

.cpp.o:
	$(CXX) $(CXXFLAGS) -c $<

clean:
	rm -f $(RESOURCE) $(OBJ) $(TARGET)

$(RESOURCE): Resource.h

$(OBJ): $(TARGET:%.exe=%.h) Resource.h
