========================== GDICMTest02 readme ==========================

GDI（デバイスコンテキスト）ベースのカラーマネジメント、およびカラープロ
ファイルの列挙のサンプルです。

"Enable ICM"のチェックでカラーマネジメント表示が有効になります。

コンボボックスにて任意のプロファイルを選択すると、描画対象（ここではウイ
ンドウ）のデバイスコンテキストの入力カラースペースが該当プロファイルに基
づくカラースペースに変更されます(ブラシのRGB値をどのようなカラーとして解
釈するかに影響）。

このサンプルに関連するブログ記事は以下を参照してください。
http://blog.livedoor.jp/yamma_ma/archives/43764758.html
http://blog.livedoor.jp/yamma_ma/archives/8434429.html
http://blog.livedoor.jp/yamma_ma/archives/8414849.html

Copyright (C) 2015 Y.Yamakawa