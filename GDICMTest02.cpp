// GDICMTest02.cpp

#include "GDICMTest02.h"
#include <windows.h>
#include <wingdi.h>
#include <icm.h>

#define MAX_LOADSTRING 100

// グローバル変数:
HINSTANCE hInst;                     // 現在のインターフェイス
TCHAR szTitle[MAX_LOADSTRING];       // タイトル バーのテキスト
TCHAR szWindowClass[MAX_LOADSTRING]; // メイン ウィンドウ クラス名


ATOM             MyRegisterClass(HINSTANCE hInstance);
BOOL             InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void             setColorProfiles(PENUMTYPE, HWND);


int APIENTRY _tWinMain(HINSTANCE hInstance,
                       HINSTANCE hPrevInstance,
                       LPTSTR    lpCmdLine,
                       int       nCmdShow)
{
  UNREFERENCED_PARAMETER(hPrevInstance);
  UNREFERENCED_PARAMETER(lpCmdLine);

  MSG msg;
  HACCEL hAccelTable;

  LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
  LoadString(hInstance, IDC_GDICMTEST02, szWindowClass, MAX_LOADSTRING);
  MyRegisterClass(hInstance);

  if (!InitInstance (hInstance, nCmdShow))
  {
    return FALSE;
  }

  hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_GDICMTEST02));

  while (GetMessage(&msg, NULL, 0, 0))
  {
    if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }
  }

  return (int) msg.wParam;
}



ATOM MyRegisterClass(HINSTANCE hInstance)
{
  WNDCLASSEX wcex;

  wcex.cbSize = sizeof(WNDCLASSEX);

  wcex.style         = CS_HREDRAW | CS_VREDRAW;
  wcex.lpfnWndProc   = WndProc;
  wcex.cbClsExtra    = 0;
  wcex.cbWndExtra    = 0;
  wcex.hInstance     = hInstance;
  wcex.hIcon         = 0;
  wcex.hCursor       = LoadCursor(NULL, IDC_ARROW);
  wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW);
  wcex.lpszMenuName  = 0;
  wcex.lpszClassName = szWindowClass;
  wcex.hIconSm       = 0;

  return RegisterClassEx(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // グローバル変数にインスタンス処理を格納します。

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX,
                       CW_USEDEFAULT, 0, 512, 432, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
  static DWORD samples[] = { 
    0xfff4f6, 0xfff4fb, 0xfef4ff, 0xfbf4ff, 0xf6f4ff, 0xf3f4ff, 0xf4f9ff, 0xf4fefe, 0xf4fffd, 0xf4fff9, 0xf3fff4, 0xf7fff4, 0xfcfff4, 0xfffef4, 0xfffbf4, 0xfff5f4,
    0xffd6dc, 0xffd6f1, 0xfdd6fe, 0xf1d6ff, 0xded6ff, 0xd6d8ff, 0xd5e9ff, 0xd6fbff, 0xd5fff7, 0xd6ffe7, 0xd6ffd8, 0xe0ffd6, 0xf2ffd6, 0xfefed6, 0xffefd6, 0xffdcd6,
    0xffb1bd, 0xffb1e4, 0xfcb1fe, 0xe4b1ff, 0xc3b1ff, 0xb1b5ff, 0xb1d6ff, 0xb1f7fe, 0xb1fff1, 0xb1ffd1, 0xb1ffb5, 0xc4ffb1, 0xe7ffb1, 0xfdfcb1, 0xffe1b1, 0xffbcb1,
    0xff8698, 0xff86d5, 0xfb86fd, 0xd486ff, 0xa286ff, 0x868dff, 0x86bfff, 0x86f3fe, 0x86ffe9, 0x86ffb7, 0x86ff8c, 0xa3ff86, 0xd9ff86, 0xfcfa86, 0xffcf86, 0xff9786,
    0xff5771, 0xff57c5, 0xf957fd, 0xc357ff, 0x7e56ff, 0x5761ff, 0x57a6ff, 0x57eefe, 0x57ffe0, 0x57ff9b, 0x57ff60, 0x80ff56, 0xcaff57, 0xfcf857, 0xffbd57, 0xff6f57,
    0xff2546, 0xff25b4, 0xf825fc, 0xb225ff, 0x5724ff, 0x2431ff, 0x258cff, 0x25e9fe, 0x25ffd7, 0x25ff7d, 0x24ff30, 0x5aff24, 0xbbff25, 0xfbf725, 0xffaa25, 0xff4325,
    0xf20025, 0xf2009e, 0xe900ef, 0x9c00f2, 0x3800f2, 0x000ff2, 0x0072f2, 0x00d9f2, 0x00f3c5, 0x00f263, 0x00f20c, 0x3bf200, 0xa7f200, 0xede900, 0xf29300, 0xf22200,
    0xc0001e, 0xc0007d, 0xb800bd, 0x7b00c0, 0x2c00c0, 0x000cc0, 0x005abf, 0x00acbf, 0x00c09b, 0x00c04e, 0x00c00a, 0x30c000, 0x84c000, 0xbcb800, 0xc07400, 0xc01b00,
    0x8f0016, 0x8f005d, 0x8a008d, 0x5d008f, 0x21008f, 0x00098f, 0x00438f, 0x00808e, 0x008f75, 0x008f3a, 0x008f08, 0x238f00, 0x628f00, 0x8c8900, 0x8f5700, 0x8f1400,
    0x620010, 0x620040, 0x5e0060, 0x3f0063, 0x170062, 0x000661, 0x002e62, 0x005861, 0x006250, 0x006228, 0x006205, 0x186200, 0x436100, 0x605e00, 0x623c00, 0x620e00,
    0x3a0009, 0x3a0026, 0x380039, 0x25003a, 0x0d003a, 0x00043a, 0x001c3a, 0x00343a, 0x003a2f, 0x003a18, 0x003a03, 0x0e3a00, 0x283a00, 0x393800, 0x3a2300, 0x3a0900,
    0x180004, 0x180010, 0x170018, 0x0f0018, 0x060018, 0x000218, 0x000b18, 0x001618, 0x001813, 0x00180a, 0x001801, 0x061800, 0x101800, 0x171700, 0x180f00, 0x180300,
    0x000000, 0x000000, 0xff0000, 0xff0000, 0x00ff00, 0x00ff00, 0x0000ff, 0x0000ff, 0xffff00, 0xffff00, 0x00ffff, 0x00ffff, 0xff00ff, 0xff00ff, 0xffffff, 0xffffff
  };

  static int nSamples = sizeof(samples) / sizeof(DWORD);
  static int margin;

  static TCHAR monitorName[CCHDEVICENAME] = { 0 }; // モニタのデバイス名
  static TCHAR outputProfileName[2048] = { 0 };    // 出力カラースペースのプロファイル

  int wmId, wmEvent;
  PAINTSTRUCT ps;

  HFONT hfont = NULL;

  HDC hdc;
  static TCHAR profileName[2048] = { 0 };

  static BOOL enableICM = TRUE;

  switch (message)
  {
  case WM_COMMAND:
    wmId    = LOWORD(wParam);
    wmEvent = HIWORD(wParam);

    switch (wmId)
    {
    case ID_CHECKBOX1:
      enableICM = (IsDlgButtonChecked(hWnd, ID_CHECKBOX1) == BST_CHECKED);
      InvalidateRect(hWnd, NULL, FALSE);
      UpdateWindow(hWnd);
      break;
    case ID_COMBO1:
      if (wmEvent == CBN_SELCHANGE)
      {
        int n = SendDlgItemMessage(hWnd, ID_COMBO1, CB_GETCURSEL, 0, 0);

        if (n == 0)
        {
          profileName[0] = 0;
        }
        else
        {
          GetDlgItemText(hWnd, ID_COMBO1, profileName, 2047);
        }

        InvalidateRect(hWnd, NULL, FALSE);
        UpdateWindow(hWnd);
      }
      break;
    default:
      return DefWindowProc(hWnd, message, wParam, lParam);
    }
    break;
  case WM_CREATE:
    {
      LOGFONT lf;
      SIZE s = { 0 };
      RECT wndRect, clientRect;

      SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(LOGFONT), &lf, 0);

      hfont = CreateFontIndirect(&lf);
      SendMessage(hWnd, WM_SETFONT, (WPARAM)hfont, TRUE);
      hdc = GetDC(hWnd);
      GetTextExtentPoint32(hdc, TEXT("__ Enable ICM"), 13, &s);
      ReleaseDC(hWnd, hdc);

      HWND hChkbox = CreateWindow(TEXT("BUTTON"), TEXT("Enable ICM"), WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, 8, 8 + s.cy / 4, s.cx, s.cy, hWnd, (HMENU)ID_CHECKBOX1, ((LPCREATESTRUCT)lParam)->hInstance, NULL);

      if (hChkbox)
      {
        SendMessage(hChkbox, WM_SETFONT, (WPARAM)hfont, TRUE);
        SendMessage(hChkbox, BM_SETCHECK, BST_CHECKED, 0);
      }

      HWND hCombobox = CreateWindow(TEXT("COMBOBOX"), TEXT(""), WS_CHILD | WS_VISIBLE | WS_VSCROLL | CBS_DROPDOWNLIST, s.cx + 16, 8, s.cx * 2, s.cy * 10, hWnd, (HMENU)ID_COMBO1, ((LPCREATESTRUCT)lParam)->hInstance, NULL);

      if (hCombobox)
      {
        SendMessage(hCombobox, WM_SETFONT, (WPARAM)hfont, TRUE);
        SendMessage(hCombobox, CB_ADDSTRING, 0, (LPARAM)TEXT("[Unspecified]"));
        SendMessage(hCombobox, CB_SETCURSEL, 0, (LPARAM)0);
        ENUMTYPE et = { 0 };
        et.dwSize = sizeof(ENUMTYPE);
        et.dwVersion = ENUM_TYPE_VERSION;
        et.dwDataColorSpace = SPACE_RGB;
        et.dwDeviceClass = CLASS_MONITOR;
        et.dwFields = ET_DATACOLORSPACE | ET_DEVICECLASS;
        setColorProfiles(&et, hCombobox); // 表示デバイスクラスのRGBプロファイルを列挙
        et.dwDeviceClass = CLASS_PRINTER;
        setColorProfiles(&et, hCombobox); // 出力デバイスクラスのRGBプロファイルを列挙
      }

      GetWindowRect(hWnd, &wndRect);
      GetClientRect(hWnd, &clientRect);

      margin = s.cy + s.cy / 2 + 16;

      SetWindowPos(hWnd, NULL, 0, 0, 512 + (wndRect.right - wndRect.left - (clientRect.right - clientRect.left)), (margin + (nSamples / 16) * 32) + (wndRect.bottom - wndRect.top - (clientRect.bottom - clientRect.top)), SWP_NOMOVE | SWP_NOZORDER);
    }
    break;
  case WM_PAINT:
    hdc = BeginPaint(hWnd, &ps);
    {
      SaveDC(hdc);

      HGDIOBJ brush = GetStockObject(DC_BRUSH); // SetDCBrushColor() で塗りつぶしのRGB値を指定
      HGDIOBJ   pen = GetStockObject(NULL_PEN);
      SelectObject(hdc, brush);
      SelectObject(hdc, pen);

      HCOLORSPACE hcs = NULL;

      // 入力カラースペースの選択
      if (profileName[0] != 0)
      {
        LOGCOLORSPACE colorspace = { 0 };
        colorspace.lcsSignature = LCS_SIGNATURE;
        colorspace.lcsSize = sizeof(LOGCOLORSPACE);
        colorspace.lcsVersion = 0x00000400;

        colorspace.lcsCSType = LCS_CALIBRATED_RGB;
        _tcsncpy(colorspace.lcsFilename, profileName, (sizeof(colorspace.lcsFilename) / sizeof(TCHAR)) - 1);
        colorspace.lcsFilename[(sizeof(colorspace.lcsFilename) / sizeof(WCHAR)) - 2] = 0;

        colorspace.lcsIntent = LCS_GM_GRAPHICS;
      
        if (hcs = CreateColorSpace(&colorspace)) SetColorSpace(hdc, hcs);
      }

      // 出力カラースペースの設定
      SetICMProfile(hdc, outputProfileName);

      // ICMのオン／オフ
      SetICMMode(hdc, enableICM ? ICM_ON : ICM_OFF);

      // 描画
      for (int i = 0; i < nSamples; i++) {
        DWORD c = samples[i];
        COLORREF cc = (COLORREF)(c >> 16 | (c & 0xff00) | (c & 0xff) << 16);
        SetDCBrushColor(hdc, cc);
        int x = (i % 16) * 32;
        int y = i / 16 * 32 + margin;
        Rectangle(hdc, x, y, x + 33, y + 33);
      };

      RestoreDC(hdc, -1);

      if (hcs) DeleteColorSpace(hcs);
    }
    EndPaint(hWnd, &ps);
    break;
  case WM_SIZE:
    if (wParam != SIZE_MINIMIZED)
    {
  case WM_MOVE:
      if (IsWindowVisible(hWnd))
      {
        // 最小化、または非表示の場合は以下を実行しない
        MONITORINFOEX info;
        info.cbSize = sizeof(MONITORINFOEX);

        HMONITOR hm = MonitorFromWindow(hWnd, MONITOR_DEFAULTTONEAREST);

        if (GetMonitorInfo(hm, &info))
        {
          if (_tcscmp(monitorName, info.szDevice) != 0)
          {
            DWORD count = 2048;

            _tcscpy(monitorName, info.szDevice);

            if (hdc = CreateIC(_T("DISPLAY"), monitorName, NULL, NULL))
            {
              GetICMProfile(hdc, &count, outputProfileName);

              DeleteDC(hdc);

              // ICM無効時は再描画の必要なし
              if (enableICM) InvalidateRect(hWnd, NULL, FALSE);
            }
          }
        }
      }
    }
    return DefWindowProc(hWnd, message, wParam, lParam);
  case WM_DESTROY:
    if (hfont) DeleteObject(hfont);
    PostQuitMessage(0);
    break;
  default:
    return DefWindowProc(hWnd, message, wParam, lParam);
  }
  return 0;
}

// 指定の条件でカラープロファイル（WCSプロファイルが含まれる）を検索し
// コンボボックスに追加する
void setColorProfiles(PENUMTYPE et, HWND hCombo)
{
  DWORD size, count;
  EnumColorProfiles(NULL, et, NULL, &size, &count);
  BYTE *buf = new BYTE[size];
  if (EnumColorProfiles(NULL, et, buf, &size, &count))
  {
    int pos1 = 0, pos2 = 0;
    TCHAR *profileNames = (TCHAR *)buf;

    while(1) {
      while(profileNames[pos1 + pos2] != 0 && pos2 < MAX_PATH) {
        pos2++;
      }

      if (pos2 == 0 || pos2 >= MAX_PATH)
      {
        break;
      }

      SendMessage(hCombo, CB_ADDSTRING, 0, (LPARAM)(profileNames + pos1));

      pos1 += pos2 + 1;
      pos2 = 0;
    }
  }
  delete buf;
}

// MinGWでUnicode版アプリケーションを作成する場合の対策用
#if defined _UNICODE | defined UNICODE
#ifndef _tWinMain
int main(int argc, char **argv)
{
    HINSTANCE hInstance = GetModuleHandle(NULL);
    int returnVal = 0;

    returnVal = _tWinMain(hInstance, NULL, _T(""), SW_SHOW);

    return returnVal;
}
#endif
#endif
